The following 2 libraries provide the tools/features you will need to fulfill analytics requirements for you page/application:

# libs/shared/page-data

This shared library has following important services, component and decorator to help you create analytics data for your app/components. You just need to import and use them as you would for any other angular service/component library.

###PageAnalyticsService
This service is the main API for analytics data generation for applications in tmo-one-site mono repo in DW3.0.
It provides the functionalities to create, initialize and put in place the observables which are effective for the user session on the site.

_Analytics Initialization:_
Invoking `initializeAnalytics()` from app component `ngOnInit` as shown below

`ngOnInit() { this.pageAnalyticsService.initializeAnalytics();`

will do the following:

- load uno utag script
- create the basic structure and initialize `window.digitalData` with initial values for: `page, user, product, search, events[]` data.
- set up observables for page navigation as defined by analytics requirements to:
  - fire a call to `window.notifyAnalytics()` upon utag script load and successful page navigation.
  - create page navigation / page load event in `window.digitalData.event[]` and invoke `window.triggerEvent()`
  - update page data whenever any url change happens.

###DigitalDataService
This service provides utility functions for PageAnalytics service and components:

- to act as in interface with DOM to access/set/update the values for: `window.digitalData` for `page, user, product, search, etc.`
- as new attributes are added to `window.digitalData` e.g. `cart`, the corresponding utility functions to set/update are created here.
- exposes the `window.digitalData`'s current value for consumption inside components (maybe used for some event data)

###ClickEventService
This service encapsulates the important functions for creating and populating click event objects in `window.digitalData.events[]`.

![](/images/click_event_service.png)

As a developer you don't need to call these functions directly, instead you should use the following directives and decorators for creating and pushing the click events:

- _ClickEventDirective_ is an Angular Attribute Directive which you can use to create and push a click event to the DOM. At present, it only listens to `click` event on the element, but in future it can be expanded to listen to other DOM events as well.

![](/images/click-event-directive.png)

**Use Cases:** HTML elements and Simpler Angular Material Components:
![](/images/anchor.png)

![](/images/div-tag.png)

![](/images/button-tag.png)

![](/images/span.png)

![](/images/material-radio-button.png)

- _ClickTracker_ is an Angular Decorator which you can use to create and push a click event to the DOM.

![](/images/click-tracker-decorator.png)

**Use Case:** Handler functions, complex Angular Material Components (for e.g. tabs), Modal load/close, Page Navigation.

![](/images/handler1.png)

![](/images/handler2.png)

![](/images/mat-dialog-open1.png)

![](/images/mat-dialog-open2.png)

![](/images/mat-dialog-close1.png)

![](/images/mat-dialog-close2.png)

![](/images/mat-tab-group1.png)

![](/images/mat-tab-group2.png)

- _NonClickEventDirective_ is another Angular Attribute Directive which you can use to create and push a click event to the DOM. It doesn't listen to any event and can be just used for UI components for which special events are requested if they are present.
  ![](/images/message-event-directive.png)

###PageDataComponent
The sole purpose of this component is to support the authoring needs for some page level attributes,
like: `pageName, pageType, primaryCategory` and `subCategory`

These values once authored in AEM are sent to the page via page partials and consumed by this component on the page. This is an angular element which enables it to be rendered in AEM for author-ability.

# libs/shared/analytics

This library contains the following directives which are used for event data generation for analytics as described above for _ClickEventService_, among others.
###ClickEventDirective
###NonClickEventDirective
