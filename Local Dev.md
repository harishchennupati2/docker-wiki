[Home](Home.md)

# Local Development

Certain scenarios require more elaborate local setups to develop and test.  

Examples:
* Cookie development and testing for cookies that have the `t-mobile.com` domain
* URL Host based development and testing - e.g. `es.t-mobile.com` or `business.t-mobile.com`
* [Insert Other Scenarios]

The basic problem set is "Domain Dependency".

This can be resolved on local as follows:

Step 1. Modify local `hosts` file to resolve `local.t-mobile.com` to `127.0.0.1`

On real operating systems this just requires a quick edit to the `/etc/hosts` file.  If you are on Windows, google it.
This will forward requests for `local.t-mobile.com` to your localhost.

Step 2. Configure server.yaml to have SSL Enabled

After Step 1 you may think you're done.  Not entirely so.  Because we use the Web Crypto API which is not available over http (Chrome blocks it).  localhost has an exception in the browser to provide this API regardless of protocol, but when accessing via a domain that exception no longer exists and https is required.
Edit the server.yaml for your local environment to have SSL enabled.   

Step 3.  Is there a third thing?  I feel like there's a third thing but I can't remember what it is.

Anyway you should be able to now navigate to https://local.t-mobile.com:{hapi_server_port_number} and load your application accordingly

This should also allow you to do things like testing logic for Spanish by going to https://es.local.t-mobile.com:{hapi_server_port_number}.  Fairly certain this being a sub-sub domain it will resolve properly with the single hosts file edit from Step 1, if it does not simply add another entry for this domain.
