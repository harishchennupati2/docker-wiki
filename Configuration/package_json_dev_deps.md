
[Home](../Home.md)

# Package.json: `devDependencies` section

## Investigate
---

* `@ngrx/store-devtools` 8.1.0: "Developer tools for @ngrx/store; E.g., Chrome/Firefox Redux Devtools extension" (5 clients)
    * If this is in devDependencies, how do the app modules pull it in on a production build?
    * apps/dam/client/src/app/app.module.ts:import { StoreDevtoolsModule } from '@ngrx/store-devtools';
    * apps/flex/client/src/app/app.module.ts:import { StoreDevtoolsModule } from '@ngrx/store-devtools';
    * apps/pub/client/src/app/app.module.ts:import { StoreDevtoolsModule } from '@ngrx/store-devtools';
    * apps/shop/client/src/app/app.module.ts:import { StoreDevtoolsModule } from '@ngrx/store-devtools';
    * tools/schematics/app/files/client/src/app/app.module.ts__tmpl__:import { StoreDevtoolsModule } from '@ngrx/store-devtools';
* The following call "import 'karma-mocha-test-setup';" but that npm package does not exist...
    * apps/dam/client/src/polyfills.ts
    * apps/flex/client/src/polyfills.ts
    * apps/shop/client/src/polyfills.ts
    * apps/style-guide/client/src/polyfills.ts
    * libs/flex/dns/feature/src/test.ts
    * libs/shared/crypto/src/test.ts
    * libs/shared/custom-elements-server/src/test.ts
    * libs/shared/custom-elements/src/test.ts
    * libs/shared/page-data/src/test.ts
    * libs/shared/search-results/feature/src/test-setup.ts
    * libs/shared/ui/hero/src/test.ts
    * libs/shop/marketing-banner/src/test.ts
    * libs/shop/plan-selector/feature/src/test.ts
    * libs/shop/pre-screen/feature/src/test.ts
    * libs/shop/product-details/feature/src/test.ts
    * libs/shop/product-list/feature/src/test.ts
    * libs/shop/product-list/ui/src/test.ts
    * libs/store-locator/feature-routes/src/test.ts
    * libs/store-locator/feature/landing-page/src/test.ts
    * libs/store-locator/feature/store-results/src/test.ts
* `minimist` 1.2.0:
    * Note: should this move to dependencies since a libs file uses it??
    * Investigate: how does this work in prod??
    * libs/hapi/util/server/src/lib/server-utils.ts
    * tools/libs/bitbucket.js
    * tools/libs/getContributors.js
    * tools/libs/selectReviewers.js
    * tools/merge/merge.js
    * tools/swagger-codegen.js

## Tech Debt
---
* The two libs below should be using lodash-es instead of lodash
    * libs/shared/data-access/geolocation/src/lib/+state/geolocation.facade.ts
    * libs/store-locator/map-utils/src/lib/map-util.service.ts
* we use `request-promise`, should we migrate to `request-promise-native`? Or just use native promises directly?
    * Note: there is also `@types/request-promise-native`
* we use `rally` 2.1.3, but Rally has been retired
    * need a story to strip out all the Rally stuff
    * tools/libs/rally-tools.js
    * tools/git/hooks/verify-rally-credentials.js
    * tools/git/validation/commit-message.js
* we use `pretty-quick` "Runs Prettier on your changed files"
    * in `.huskyrc`:  "pre-commit": "pretty-quick --staged"
    * could this be converted to some flavor of `nx format:write`?

## Candidates for Removal
---

* `awesome-typescript-loader` 5.2.1: "Awesome TS loader for webpack" -- `ts-loader` replacement (0 clients)
    * not being used -- Nrwl builders in angular.json control the build...
    * and the `webpackConfig` override is `tools/custom-lodash-webpack.config.js`, which only pulls in 'webpack-node-externals'
* `axe-core` 3.1.2: "Accessibility engine for automated Web UI testing" (0 clients)
    * was being used, but can REMOVE now
* `axe-reports` 1.1.11: "Create human readable reports from the results object created by the aXe analyze function." (0 clients)
* `concurrently` 4.0.1: "Run commands concurrently" (0 clients)
    * we use `concurrently` via `nps-utils`, but it has its own copy
* `deepmerge` 3.1.0: "A library for deep (recursive) merging of Javascript objects" (0 clients)
    * no direct clients, and these packages have their own copy:
        * swagger-client
        * tsconfig-paths-webpack-plugin
        * ts-mocha
* `dotenv` 6.2.0: "Loads environment variables from .env file" (0 clients)
* `nconf` 0.10.0: "Hierarchical node.js configuration w/ files, env vars, CLI args" (0 clients)
* `request-promise` 4.2.4:
    * this is already in 'dependencies', to support `libs/hapi/util/taap/src/lib/jwt-validator.js`

## Questions
---

* `@types/request` 2.48.1:
    * but we use `request-promise`... (which is in both dependencies *and* devDependencies)
    * there is a `@types/request-promise`, add that to dependencies?
    * Dependencies for both: `@types/request`, `@types/bluebird` <-- (add?)
    * there is a `@types/request-promise-native`, migrate to that?

## Valid devDependencies
---
```
  @angular-devkit/build-angular 0.802.1: "Angular Webpack Build Facade" (3 clients)
    angular.json
    karma.conf.js
    tools/schematics/app/index.ts
  @angular/cli 8.1.1: "CLI tool for Angular" (3 clients)
    angular.json
    package-scripts-utils.js
    setup.json
  @angular/compiler-cli 8.2.0: Angular - the compiler CLI for Node.js (0 clients)
  @angular/language-service ^8.2.0: provides code editors with completions, errors, hints, and navigation inside Angular templates. (0 clients)

  @ngrx/schematics 8.1.0: NgRx Schematics for Angular (0 clients)

  @nrwl/jest 8.4.6: Jest plugin for Nx (2 clients)
    angular.json
    jest.config.js
  @nrwl/workspace 8.4.6: Extensible Dev Tools for Monorepos (4 clients)
    angular.json
    package-scripts.js
    tools/schematics/api/index.js
    tslint.json

  @types/chai 4.1.7:
  @types/express 4.16.0:
  @types/fs-extra 5.0.4:
  @types/hammerjs 2.0.36:
  @types/hapi__boom 7.4.1:
  @types/hapi__hapi 18.2.6:
  @types/ioredis 4.0.4:
  @types/jest 24.0.9:
  @types/jquery 3.3.29:
  @types/lodash 4.14.120:
  @types/mocha 5.2.5:
  @types/node 12.7.0:
  @types/sinon 7.0.5:
  @types/sinon-chai 3.2.2:
  @types/sinon-test 1.0.5:

  allure-commandline 2.8.1: NPM wrapper around allure-commandline; Allure (requires Java 8+) creates reports from test results. (0 clients)
  --> Q: do we use Allure somewhere? A: keep it just in case
  -- 'allure' shows up:
       .gitignore: **/allure-report
       .gitignore: **/allure-results
       .prettierignore: allure-results

  chai 4.2.0: BDD/TDD assertion library for node.js and the browser. Test framework agnostic. (163 clients)
  chai-as-promised 7.1.1: Extends Chai with assertions about promises (1 client)
    tools/libs/bitbucket.mocha.js

  chai-jest-diff 1.0.2: A plugin to make Chai’s equality assertions (shallow or deep) fail with Jest-like diffs (1 client)
    global-chai-setup.ts

  chokidar 2.1.2: A neat wrapper around node.js fs.watch / fs.watchFile / fsevents (1 clients)
    tools/s3-watch.js

  codelyzer ^5.0.1: A set of tslint rules for static code analysis of Angular TypeScript projects (1 clients)
    tslint.json

  colors 1.3.3: Get colors in your node.js console (21 clients)

  cross-env 5.2.0: Run scripts that set and use environment variables across platforms (1 clients)
      .huskyrc (node_modules/.bin/cross-env-shell)

  document-register-element 1.8.1: A stand-alone working lightweight version of the W3C Custom Elements specification (7 clients)
    angular.json
    libs/shared/analytics/src/test-setup.ts
    libs/shared/search-results/data-access/src/test-setup.ts
    libs/shared/search-results/ui/src/test-setup.ts
    libs/shop/pre-screen/model/src/test-setup.ts
    libs/shop/pre-screen/ui/src/test-setup.ts
    tools/schematics/app/index.ts

  file-loader 2.0.0: A file loader module for webpack (0 clients) - used by Webpack under the hood?

  fs-extra 7.0.0: contains methods that aren't included in the vanilla Node.js fs package (5 clients)
    libs/hapi/util/server/src/lib/server-utils.service.ts
    tools/cd/merge-coverage.js
    tools/ci/update-pull-request-status.js
    tools/merge-swagger-files.js
    tools/post-hydrate.js

  husky 1.1.3: Git hooks made easy (1 client)
    .huskyrc

  intersection-observer 0.7.0: A polyfill for IntersectionObserver (4 clients)
    --> fun fact: we have implemented a IntersectionObserverService that assumes IntersectionObserver is defined.
    apps/dam/client/src/polyfills.ts:import 'intersection-observer';
    apps/flex/client/src/polyfills.ts:import 'intersection-observer';
    apps/pub/client/src/polyfills.ts:import 'intersection-observer';
    apps/shop/client/src/polyfills.ts:import 'intersection-observer';

  istanbul-lib-coverage: API that provides a read-only view of coverage information with the ability to merge and summarize coverage info (1 clients)
    tools/cd/merge-coverage.js
  istanbul-lib-report 2.0.8: Base reporting library for istanbul (1 client)
    tools/cd/merge-coverage.js
  istanbul-reports 2.2.4: Istanbul reports (1 client)
    tools/cd/merge-coverage.js

  jasmine-marbles 0.4.0: Marble testing helpers for RxJS and Jasmine (17 clients)

  jest 24.8.0: Delightful JavaScript Testing (7 clients, plus **/tsconfig.json and **/tsconfig.spec.json)
    angular.json
    libs/hapi/apigee-access-token/src/lib/apigee-access-token.service.spec.ts
    libs/hapi/domain/cfs/src/lib/key-repository.spec.ts
    libs/hapi/domain/mytmo/src/lib/customer-shop-eligibility-repository.spec.ts
    libs/hapi/domain/mytmo/src/lib/user-credentials.repository.spec.ts
    tools/schematics/api/index.js
    tsconfig.json
  jest-preset-angular 7.1.1: Jest preset configuration for Angular projects (many clients)
    **/jest.config.js
    **/test-setup.ts

  jfile 1.1.12: OOP way to handles Files. It can behave like `java.io.File` (1 client)
    tools/setup/setup.js

  karma 4.0.1: Spectacular Test Runner for JavaScript (1 client)
    karma.conf.js
  karma-chrome-launcher 2.2.0: A Karma plugin. Launcher for Chrome and Chrome Canary (1 client)
    karma.conf.js
  karma-coverage-istanbul-reporter 2.0.4: A karma coverage reporter that uses the latest istanbul 1.x APIs (1 clients)
    karma.conf.js
  karma-mocha 1.3.0: A Karma plugin. Adapter for Mocha testing framework. (x clients)
    karma.conf.js
  karma-mocha-reporter 2.2.5: Karma reporter with mocha style logging (1 client)
    karma.conf.js
  karma-sinon-chai 2.0.2: Sinon and Chai for Karma (1 client)
    karma.conf.js


  livereload 0.8.0: implementation of the LiveReload server in Node.js (6 clients)
    apps/dam/livereload/src/main.ts
    apps/flex/livereload/src/main.ts
    apps/pub/livereload/src/main.ts
    apps/shop/livereload/src/main.ts
    apps/style-guide/livereload/src/main.ts
    tools/schematics/app/files/livereload/src/main.ts__tmpl__

  lodash 4.17.13: Lodash modular utilities (11 clients)
    libs/shared/data-access/geolocation/src/lib/+state/geolocation.facade.ts
    libs/store-locator/map-utils/src/lib/map-util.service.ts
    tools/ci/update-build-status.js
    tools/ci/update-pull-request-status.js
    tools/libs/bitbucket.js
    tools/libs/bitbucket.mocha.js
    tools/libs/bitbucketV1.js
    tools/libs/gitQuery.js
    tools/libs/rally-tools.js
    tools/libs/selectReviewers.js
    tools/merge/merge.js

  mocha 5.2.0: Simple, flexible, fun test framework (6 clients and **/tsconfig.json)
    karma.conf.js
    libs/shared/client-e2e/src/lib/configure.ts
    tools/run-mocha-test-for-project.js
    tools/schematics/app/files/server/tsconfig.json__tmpl__
    tools/tsconfig.tools.json
    tsconfig.json

  mustache 3.1.0: Logic-less {{mustache}} templates with JavaScript (1 client)
    tools/partials/create-partials.js

  nps 5.9.8: All the benefits of npm scripts without the cost of a bloated package.json and limits of json (5 clients)
    master.jenkinsfile
    package-scripts-utils.js
    package-scripts.js
    tools/ci/affected.js
    tools/start.js

  nps-utils 1.7.0: Utilities for nps (3 clients)
    package-scripts-utils.js
    package-scripts.js
    tools/patch-nps-utils.js

  prettier 1.16.4: an opinionated code formatter (0 clients)
    -- no direct clients, but is a peerDependency of @nrwl/workspace (nx format:check|write)
    -- and might be used by IDEs

  pretty-quick 1.11.1: Runs Prettier on your changed files (1 clients)
    .huskyrc:  "pre-commit": "pretty-quick --staged"

  protractor ~5.4.0: Webdriver E2E test wrapper for Angular (17 clients)

  proxyquire 2.1.0: Proxies nodejs require in order to allow overriding dependencies during testing (1 client)
    tools/libs/bitbucket.mocha.js

  pwmetrics 4.2.1: Gather progressive web metrics for a url. CLI & lib (2 clients)
    package-scripts.js
    tools/pwmetrics.config.js

  rally 2.1.3: Rally REST Toolkit for Node.js (1 client, 2 indirect clients)
    tools/libs/rally-tools.js
     |- tools/git/hooks/verify-rally-credentials.js
     \- tools/git/validation/commit-message.js

  readline-sync 1.4.9: Synchronous Readline for interacting woth user via console (3 clients)
    tools/libs/gitQuery.js
    tools/libs/rally-tools.js
    tools/merge/merge.js

  recast 0.16.2: JavaScript syntax tree transformer, nondestructive pretty-printer, and automatic source map generator (1 client)
    tools/schematics/app/index.ts

  redis-mock 0.43.0: Redis client mock object for unit testing (3 clients)
    libs/hapi/apigee-access-token/src/lib/apigee-access-token.service.spec.ts
    libs/hapi/domain/cfs/src/lib/key-repository.spec.ts
    libs/hapi/util/redis-client/src/lib/redis-client.spec.ts

  request 2.88.0: Simplified HTTP request client (x clients)
    tools/libs/bitbucket.js
    tools/libs/bitbucketV1.js

  request-debug 0.2.0: Library to assist with debugging HTTP(s) requests made by the request module (1 client)
    tools/libs/bitbucket.js

  shelljs 0.8.3: Portable Unix shell commands for Node.js (27 clients)

  sinon 7.2.2: JavaScript test spies, stubs and mocks (85 clients, 84 are spec files, 1 is tools/libs/bitbucket.mocha.js)
  sinon-chai 3.3.0: Extends Chai with assertions from Sinon.JS (51 clients)
  sinon-test 2.4.0: Automatic sandbox setup and teardown for SinonJS (1 client)
    libs/shared/util/chat-cta/tsconfig.json

  slugify 1.3.4: Slugifies a String; Coerces foreign symbols to their English equivalent (2 clients)
    libs/shared/util/url/src/lib/url.util.ts
    tools/partials/create-partials.js

  stylelint 10.0.1: A mighty, modern CSS linter (1 client)
    package-scripts.js

  stylelint-config-standard 18.3.0: Standard shareable config for stylelint (1 client)
    .stylelintrc

  swagger-client 3.8.25: SwaggerJS - a collection of interfaces for OAI specs (2 clients)
    package-scripts-utils.js
    package-scripts.js

  ts-jest 24.0.2: A preprocessor with source maps support to help use TypeScript with Jest (16 clients)
  ts-mocha 2.0.0: Mocha thin wrapper that allows running TypeScript tests with TypeScript runtime (ts-node) to get rid of compilation complexity (1 client)
    tools/run-mocha-test-for-project.js
  ts-node 8.4.1: TypeScript execution environment and REPL for node.js, with source map support (1 client)
    apps/shop/client-e2e/protractor.conf.js

  tslint 5.13.1: An extensible static analysis linter for the TypeScript language (2 clients)
    tools/lint-rules/src/noUnusedExpressionChaiRule.ts
    tools/lint-rules/src/utils/lintRunner.spec-util.ts

  typescript 3.5.3: the typescript compiler (3 clients)
    tools/lint-rules/src/noUnusedExpressionChaiRule.ts
    tools/lint-rules/src/utils/ast-utils.ts
    tools/lint-rules/src/utils/test-framework-utils.ts

  webdev-setup-tools 5.0.0: Setup and install packages required for many web development tasks (3 clients)
    tools/setup/setup.bat
    tools/setup/setup.js
    tools/setup/setup.sh
  webdev-setup-tools-aem 5.0.0: Set up aem dependencies for development tasks (1 client)
    tools/setup/setup.js
  webdev-setup-tools-java 5.0.0: Walk through java jdk download and setup (1 client)
    tools/setup/setup.js
  webdev-setup-tools-maven 5.0.0: Setup maven for development (1 client)
    tools/setup/setup.js
  webdev-setup-tools-npm-globals 5.0.0: Set up npm packages for development tasks (1 client)
    tools/setup/setup.js

  webpack-bundle-analyzer 3.3.2: Webpack plugin and CLI utility that represents bundle content as convenient interactive zoomable treemap (0 clients)
    -- but can use via commandline "webpack-bundle-analyzer bundle/output/path/stats.json"

  xml2js 0.4.19: Simple XML to JavaScript object converter (1 client)
    tools/setup/setup.js
```
---

[Home](../Home.md)
