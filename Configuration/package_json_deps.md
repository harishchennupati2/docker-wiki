
[Home](../Home.md)

# Package.json: "New update required"

## Needs Investigation
---
* the sole client of `core-js` (below), is `karma-mocha-test-setup.ts`
    * it is imported into all the polyfills, e.g. `apps/shop/client/src/polyfills.ts`...
    * but it has testing libs, so those are being imported into the main client polyfill files
    * this should get reviewed, maybe split into two?
* `@types/googlemaps` 3.37.6 (2 clients)
    * `libs/shared/data-access/geolocation/src/lib/geolocation.service.ts`
    * `libs/store-locator/map-utils/src/lib/map-util.service.ts`
    * **Note:** the libs are using the Typescript triple slash notation, which should only be in a `*.d.ts` file...
      ```/// <reference types="@types/googlemaps" />```
      "For declaring a dependency on an `@types` package in a .ts file, use `--types` on the command line
      or in your `tsconfig.json` instead. See using `@types`, `typeRoots` and types in `tsconfig.json` files for more details."
    * https://www.typescriptlang.org/docs/handbook/triple-slash-directives.html

## Candidates for removal
---
* `brakes` 2.6.0: "Node.js Circuit Breaker Pattern" (0 clients) -- was used for deleted circuit breaker code
* `crypto-js` 3.1.9-1: "JavaScript library of crypto standards" (0 clients)
* `d3` 5.9.7: "Data-Driven Documents" (0 clients)
* `fibers` 4.0.2: "Co-routines for JS" (0 clients)
* `ngx-bootstrap` 5.1.1: "Native Angular Bootstrap Components" (0 clients)
* `ngx-captcha` 6.4.0: (0 clients)
* `ngx-clipboard` 11.1.9: (0 clients)
* `ngx-logger` 4.0.3: (0 clients)
* `ngx-virtual-scroller` 3.0.0: (0 clients)


## Deprecated, need tech debt stories
---
* ~~`circular-json` 0.5.9: "Circular JSON.stringify and JSON.parse" (4 clients)~~
    * this is **DEPRECATED**, replaced by [`flatted`](https://www.npmjs.com/package/flatted), more efficient and same API
    * Note: **completed** via [CDCSM-1238](https://jirasw.t-mobile.com/browse/CDCSM-1238)
* ~~`joi` 14.3.1: "Powerful schema description language and data validator for JavaScript" (4 clients)~~
    * this is **DEPRECATED**, now use `@hapi/joi`
    * Note: **completed** via [CDCSM-1239](https://jirasw.t-mobile.com/browse/CDCSM-1239)
* ~~`rxjs-compat` 6.5.2: "Upgrade to RxJS version 6 without having to modify code" (0 clients -- patches RxJS code)~~
    * we need to remove this package and get the code working w/o it
    * https://ncjamieson.com/avoiding-rxjs-compat/
    * Note: **completed** via [CDCSM-1240](https://jirasw.t-mobile.com/browse/CDCSM-1240)
* ~~`vision` 5.4.4: "Templates rendering plugin support for hapi.js" (1 client)~~
    * `libs/hapi/util/server/src/lib/server-utils.service.ts`
    * this is **DEPRECATED**, now use [`@hapi/vision`](https://github.com/hapijs/vision)
    * Note: (from [CDCSM-1241](https://jirasw.t-mobile.com/browse/CDCSM-1241)) `hapi-swaggered-ui` needs older vision library and is the plugin that uses it. Will not upgrade vision lib.

## Candidate for migration
---
* `request-promise` 4.2.4: "Simplified HTTP request client with Promise support" (2 clients)
    * `libs/hapi/util/taap/src/lib/jwt-validator.js`
    * `tools/merge/merge.js`
    * should we migrate from `request-promise` to [`request-promise-native`](https://github.com/request/request-promise-native)?
    * it similar to `request-promise` but uses native ES6+ promises, **instead** of Bluebird
    * Note: it is also in `devDependencies`, along with `request` and `request-debug`...


## Candidates to move into "devDependencies"
---
* `@bazel/bazel` 1.0.0: "Build and test software"
    * no imports
* `@nrwl/node` 8.4.6: "Node Plugin for Nx"
    * no imports
* `@types/lodash-es` 4.17.3
    * no direct imports, but used by `lodash-es` (below)
    * shouldn't all `@types` be in `devDependencies`?
* `aws-sdk` 2.389.0: "The SDK for Amazon Web Services"
    * cannot find any clients via `git grep`
    * even if there were, should prob be in `devDependencies`
* `blueprint-css`: 3.1.1
    * `libs/shared/tmo-styles/src/lib/_layout.scss`
    * only used by one scss file, thus be in `devDependencies`
* `bootstrap` 4.3.1: "popular responsive, mobile-first front-end framework" (1 client)
    * `apps/pub/client/src/styles.scss`
    * since client is an import to sccs file, should be in `devDependencies`
* `core-js`: 2.6.2: "Modular standard lib for JS. Includes polyfills for ECMAScript up to 2019" (1 client)
    * `karma-mocha-test-setup.ts`
    * since it is polyfills, should be in `devDependencies`
* `dtsgenerator` 2.0.5: "Typescript d.ts file generator" (1 client)
    * `tools/swagger-codegen.js`
    * since it is used by a tool, should be in `devDependencies`
* `font-awesome` 4.7.0: "The iconic font and CSS framework" (3 clients)
    * `angular.json`
    * `apps/pub/client/src/styles.scss`
    * `apps/pub/client/src/styles.scss`
    * since scss gets compiled, should go into `devDependencies`
* `inquirer` 6.2.1: "A collection of common interactive command line user interfaces" (2 clients)
    * `tools/libs/jira-tools.js`
    * `tools/libs/selectReviewers.js`
    * since both are tools, move to `devDependencies`
* `intersection-observer` 0.7.0: "A polyfill for IntersectionObserver" (4 clients)
    * `apps/dam/client/src/polyfills.ts`
    * `apps/flex/client/src/polyfills.ts`
    * `apps/pub/client/src/polyfills.ts`
    * `apps/shop/client/src/polyfills.ts`
    * since it is a polyfill, move to `devDependencies`
    * golly, it *already* is in `devDependencies`!
* `node-sass` 4.12.0: "Wrapper around libsass" (0 clients)
    * scss, so move to `devDependencies`
* `normalize.css` 8.0.1: (4 clients)
    * all clients are scss files, should be in `devDependencies`
* `polyfill-array-includes` 2.0.0: (4 clients)
    * `apps/dam/client/src/polyfills.ts`
    * `apps/flex/client/src/polyfills.ts`
    * `apps/pub/client/src/polyfills.ts`
    * `apps/shop/client/src/polyfills.ts`
    * polyfills, so should go into `devDependencies`
    * Q: `core-js` implements an `Array.prototype.includes`, so is this redundant?
* `smoothscroll-polyfill` 0.4.3: "polyfill or opt in to native smooth scrolling" (1 client)
    * `angular.json`
    * `flex-client` build definition, 'scripts' section --> move to `devDependencies`
* `ts-loader` 5.3.3: "TypeScript loader for webpack" (0 clients)
    * move to `devDependencies`
* `tsconfig-paths` 3.9.0: "Load node modules according to tsconfig paths, in run-time or via API" (2 clients)
    * `apps/shop/client-e2e/protractor.conf.js`
    * `tools/run-mocha-test-for-project.js`
    * clients are e2e and a tool, move to `devDependencies`
* `tslib` 1.9.3: "Runtime library for TypeScript helper functions for optimized bundles" (0 clients)
    * since this is about compiling, move to `devDependencies`
* `url-polyfill` 1.1.7: "Polyfill URL and URLSearchParams" (4 clients)
    * `apps/dam/client/src/polyfills.ts`
    * `apps/flex/client/src/polyfills.ts`
    * `apps/pub/client/src/polyfills.ts`
    * `apps/shop/client/src/polyfills.ts`
    * polyfills, so move to `devDependencies`
* `webcrypto-shim` 0.1.5: "Web Cryptography API shim for legacy browsers" (4 clients)
    * `apps/dam/client/src/polyfills.ts`
    * `apps/flex/client/src/polyfills.ts`
    * `apps/pub/client/src/polyfills.ts`
    * `apps/shop/client/src/polyfills.ts`
    * polyfills, so move to `devDependencies`
    * Q: is only for these browsers, do we need to support?
        * Internet Explorer 11, Mobile Internet Explorer 11
        * Safari 8 - 10, iOS Safari 8 - 10.

## Valid dependencies
---
```
  @angular/animations 8.2.0: (8 clients)
  @angular/cdk 8.0.1: (12 clients)
  @angular/common 8.2.0: (322 clients)
  @angular/compiler ^8.2.0: (0 clients)
    -- used by Angular under the hood
    -- e.g., @nguniversal/hapi-engine imports from it
  @angular/core 8.2.0: (788 clients)
  @angular/elements 8.2.1: (1 direct client -> 12 indirect clients)
    -- libs/shared/custom-elements/src/lib/register-element.ts --> '@tmo/shared/custom-elements' (12 clients)
  @angular/flex-layout 8.0.0-beta.26: (29 clients)
  @angular/forms 8.2.0: (35 clients)
  @angular/material 8.0.1: (124 clients)
  @angular/material-moment-adapter 8.2.3: Used with Material Datepicker (1 client)
    -- libs/shop/pre-screen/ui/src/lib/pre-screen-form/pre-screen-form.component.ts
    -- other Datepicker uses MatNativeDate adapter
  @angular/platform-browser 8.2.0: (140 clients)
  @angular/platform-browser-dynamic 8.2.0: (40 clients)
  @angular/platform-server 8.2.0: (6 clients)
  @angular/router 8.2.0: (170 clients)
  @hapi/boom 7.4.11: "HTTP-friendly error objects" (44 clients)
  @hapi/hapi 18.2.0: "HTTP Server framework" (19 clients)
  @hapi/inert 5.2.2: "Static file and directory handlers plugin for hapi.js" (1 client)
    -- libs/hapi/util/server/src/lib/server-utils.service.ts
  @mdi/font 3.8.95: "Material design icons" (1 client)
    -- angular.json
  @ngrx/effects 8.1.0: (53 clients)
  @ngrx/entity 8.1.0: (6 clients)
  @ngrx/router-store 8.1.0: (9 clients)
  @ngrx/store 8.1.0: (117 clients)
  @nguniversal/common 8.1.1: "Angular Universal common utilities" (0 clients)
    -- no direct imports, but @nguniversal/hapi-engine imports from it
  @nguniversal/hapi-engine 9.0.0-next.6: "Hapi Engine for running Angular Apps on the server for SSR" (1 client)
    -- libs/hapi/core/src/lib/ng-plugin.ts
  @nguniversal/module-map-ngfactory-loader 8.1.1: "NgFactory Loader which uses a map of modules instead of resolving modules lazily" (7 clients)
  @nrwl/angular 8.4.6: "Angular Plugin for Nx" (43 clients -- NxModule, DataPersistence, hot, readFirst)
  @tmo/digital-unav 0.0.68: (5 clients)
    -- requires VPN to run `npm install` :-(
  axios 0.19.0: "Promise based HTTP client for the browser and node.js" (6 clients, 5 are dev but 1 is not)
  bb-benefits-ui: http://comsvn3a.eservice.t-mobile.com:8081/artifactory/eservice-britebill/bb-benefits-ui-1.0.852.tgz (1 client)
    -- libs/pub/benefits/src/lib/benefits-statement.module.ts
    -- requires VPN to run `npm install` :-(
  bb-billing-ui: http://comsvn3a.eservice.t-mobile.com:8081/artifactory/eservice-britebill/bb-billing-ui-1.0.854.tgz (1 client)
    libs/pub/billing/src/lib/billing.module.ts
    -- requires VPN to run `npm install` :-(
  flatted 2.0.0: (0 clients) -- BUT circular-json clients need to migrate to this, so keep
  hammerjs 2.0.8: "A javascript library for multi-touch gestures" (5 clients)
  hapi-openapi 1.2.4: "Design-driven apis with OpenAPI (formerly Swagger) 2.0 and hapi" (1 client)
    -- libs/hapi/util/server/src/lib/server-utils.service.ts
  hapi-swaggered-ui 3.0.2: "Easy swagger-ui drop-in plugin for hapi to be used with hapi-swaggered" (2 clients)
    -- angular.json
    -- libs/hapi/util/server/src/lib/server-utils.service.ts
  htmlparser2 3.10.1: (1 client) - just examining <script> tags and gathering src's and href's...
    -- libs/hapi/core/src/lib/static-assets-parser.ts
  ioredis 4.3.0: "full-featured Redis client for Node.js" (1 client)
    -- libs/hapi/util/redis-client/src/lib/redis-client.ts
  jsonwebtoken 8.4.0: "JSON Web Token implementation" (5 clients)
    -- Q: do we need to add @types/jsonwebtoken
  jsrsasign 8.0.12: "Opensource pure JS crypto lib, supports signing/validation" (1 client)
    -- libs/hapi/util/jwt/src/lib/jwt-utils.ts
  jwk-to-pem 2.0.1: "Convert a JSON Web Key to a PEM" (1 client)
    -- libs/hapi/util/jwt/src/lib/jwt-utils.ts
  lodash-es 4.17.13: "Lodash exported as ES modules" (105 clients)
  log4js 5.1.0: "Port of Log4j to work with node" (27 clients)
  lru-cache 5.1.1: "A cache object that deletes the least-recently-used items" (1 client)
    -- libs/hapi/util/taap/src/lib/jwt-validator.js
  moment 2.24.0: Parse, validate, manipulate, and display dates (1 client)
    -- libs/shop/pre-screen/ui/src/lib/pre-screen-form/pre-screen-form.component.ts
  ngx-swiper-wrapper 8.0.1: "Angular wrapper library for Swiper" (2 clients)
    -- libs/store-locator/ui/carousel/src/lib/carousel.component.ts
    -- libs/store-locator/ui/carousel/src/lib/store-locator-ui-carousel.module.ts
  object-hash 1.3.1: "Generate hashes from javascript objects in node and the browser" (2 clients)
    -- libs/hapi/util/cacheable/src/lib/cacheable.ts
    -- libs/hapi/util/soap-client/src/lib/soap-client.ts
  raf 3.4.1: "`RequestAnimationFrame` polyfill for node and the browser" (2 clients)
    -- apps/flex/client/src/main.server.ts
    -- apps/shop/client/src/main.server.ts
  rxjs 6.5.2: "Reactive Extensions for modern JavaScript" (187 clients)
  soap 0.25.0: "A minimal node SOAP client" (1 client)
    -- libs/hapi/util/soap-client/src/lib/soap-client.ts
  uuid 3.3.2: "RFC4122 (v1, v4, and v5) UUIDs" (11 clients)
  zone.js ~0.9.1: "Angular, Zones for JavaScript" (22 clients)
```
---

[Home](../Home.md)
