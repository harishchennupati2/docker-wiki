[Home](Home.md)

# AEM Installation and Configuration (MacOS)

## Prerequisites

* Java (must be JDK 1.8)
* Maven (i.e., 3.6.2)
    * Download: https://maven.apache.org/download.html
    * Install: https://maven.apache.org/install.html
* AEM install files:
    * `AEM Instance.zip` (AEM Server, version 6.4)
    * `aem-service-pkg-6.4.3.zip` (AEM Service Pack 3)
    * `content-1.0.zip` (T-Mobile Content Package)

## Install AEM 6.4 and Service Pack 3

* Obtain the file `AEM Instance.zip` -- this has the quickstart and license
* Double click to expand the zip
* This will create the `AEM Instance` folder - open it
* Double click the jar file `cq64-author-p4502.jar`
* This should start the AEM server in a new window
* If you get a popup that it "cannot be opened because it is from an unidentified developer":
    * Press the CTRL button while single clicking the file
    * Select `Open` from the menu that pops up, and wait several seconds
    * A popup appears warning you about the unidentified developer, but now you have an `Open` button as well as an `Cancel` button
    * Select `Open` from the popup and the server should start
    * The override will be stored in the system and in the future you can double-click the jar file to start the AEM server
* It will take a while to build files and so on -- you will see a new `crx-quickstart` folder appear inside `AEM Instance`
* When it has spun up, it should open a new browser tab -- if not, go to http://localhost:4502/aem/start.html
* Log into the server (user: `admin` / password: `admin`)

## Install AEM Service Pack 3

* Make sure the AEM Server is running
* If you are not already accessing AEM in the browser, open a new tab with http://localhost:4502/aem/start.html
* Log in, if necessary (user: `admin` / password: `admin`)
* Click the little hammer icon on the left (`Tools`)
* Click on `CRXDE Lite` (alternatively, open a browser tab to http://localhost:4502/crx/de/index.jsp)
* In the top bar, click the middle icon that looks like a cube for the package manager (http://localhost:4502/crx/packmgr/index.jsp)
* Click `Upload Package`
* Upload service pack 3 (`aem-service-pkg-6.4.3.zip`) via the dialog
* Click `Install` on the main package manager page, and a popup will appear
* Click `Install` in the popup (don't change any settings) and it will dismiss
* Wait... and wait... ... ...
* When it is done, restart the AEM server:
    * In the server window, click the slider that says `On`, it will then read `Off` and take a while to finish -- when it has, the window will go away
    * Close the browser tab(s) pointing to `localhost:4502`
    * Double click the jar file `cq64-author-p4502.jar` to start the AEM server again

## Configure AEM

### Install the Content Package

* Make sure the AEM Server is running
* Navigate to the package manager as described above, or open a tab to http://localhost:4502/crx/packmgr/index.jsp
* Click `Upload Package`
* Upload the content package (`content-1.0.zip`) via the dialog
* Click `Install` on the main package manager page, and a popup will appear
* Click `Install` in the popup (don't change any settings) and it will dismiss
* When it is done, proceed to the next section.

### Create the `tmo-repo-user` system user -- without this, you will be not able to run any workflows nor see any content in AEM

* Make sure the AEM Server is running
* Open this link: http://localhost:4502/crx/explorer/index.jsp
* **Important:** If the `UserID` shows as "anonymous":
    * Click the `Log In` link
    * User: `admin` / Password: `admin`
* Click the `User Administration` link -- a new window will appear
* Click `Create System User` -- a form appears in the window
* Enter `tmo-repo-user` in the `UserID` field
* Leave the `Intermediate Path` field empty
* Click the green checkmark
* Click the `Close` button at the bottom right to close the window
* Now navigate the browser tab to http://localhost:4502/useradmin
* In the search field, search for `tmo-repo-user`
* Single-click the resulting `tmo-repo-user` to highlight it
* Right-click and select 'Activate'
* A popup appears, click `Yes`
* Single-click the user again, to highlight it
* Double-click and info should appear on the right
* Select the `Permissions` tab
* Ensure that every single checkbox has been selected
* Click `Save` (just below the `Properties` tab)

## Build the Local

* Make sure the AEM server is running
* **Important:** make sure the Angular app is running (`npm start shop.dev`)
* `cd` to the git repo root, and then to the `aem` sub-dir
* Type `mvn clean install` -- this will download a ton of stuff and take a while
* The summary at the end of the output should indicate `SUCCESS` for each item - if not, something is wrong and needs to be fixed before proceeding
* Type `mvn clean install -PautoInstallPackage` -- this will install packages/artifacts/bundles to your CRXDE
* Again, each item in the summary should have `SUCCESS` before proceeding

## Verify the Local

* Make sure the AEM server is running
* Make sure the Angular app is running (`npm start shop.dev`)
* Navigate to http://localhost:4502/sites.html/content/tmobile/en/shop
* In the next to last column you should see "Apple & Android Cell Phones" - click the icon to the left of that text
* A checkmark appears, along with info on the right, and a new toolbar appears at the top of the page
* In that new toolbar, click `Edit (e)`
* This will open the cell-phones page (via your Angular app) in edit-mode in a new tab (http://localhost:4502/editor.html/content/tmobile/en/shop/cell-phones.html)
* You should see content similar to the T-Mobile cell phones page - if so, everything is installed OK!

## Finishing Touches

In order for your local to 'Quick Publish' partial htmls to `mock-data`, it needs to be able to resolve `/uno/tmo-one-site` as the location of the repo. There are two ways to do this:

1. Go to http://localhost:4502/system/console/configMgr and search for 'Amazon S3 Configuration'. Double click it, edit the 'Local dist folder path for AEM content', and then click 'Save'.

2. Create a symlink called `/uno` that points to your repo's parent dir. For example, if you have the repo checked out into `/Users/bobsmith/Projects/tmo-one-site`, then you would do this `sudo ln -s /Users/bobsmith/Projects /uno`

---

[Home](Home.md)
