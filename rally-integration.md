
[Home](Home.md)

# Rally Integration

Some git hooks here integrate with Rally.  In order for these to work you need a Rally API key.

You can create an ApiKey at the following _logged in_ url [Rally user account apiKeys](https://rally1.rallydev.com/login/accounts/index.html#/keys)
