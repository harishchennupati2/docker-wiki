[Home](Home.md)

Nx uses tags to restrict the dependency graph of the monorepo. The tags are defined in nx.json as part of the Nx meta data. During linting, tag restrictions will be applied based on rules defined in tslint.json (nx-enforce-module-boundaries).

Please make sure to apply the appropriate tags to any new libs or applications. Tags can be added when running the lib schematics, or retroactively by editing nx.json. Whenever a new application is added, please make sure to also update tslint.json with a new scope representing the application.

The following sections provided details about the available tags.
 
TMO supports 3 categories of tags

## Platform

The platform tag is used to specify the target platform of libs and applications.

Possible values:

### server 

Server specific code

### client

Client specific code

Examples: platform:server and/or platform:client. In cases where a lib is shared between server and client, both tags are applied.

## Scope

Scope is a domain specific grouping of libs and applications. The purpose of defining a scope is to avoid unintended dependencies across application boundaries. As an example, we don’t want shop specific code to depend on style-guide specific code. In cases where sharing is desirable, a lib should be tagged with scope:shared.

Possible values:

### shared 

scope:shared is for code that can be shared across the entire monorepo. 

### Application specific domains

scope:[INSERT app specific-domain] is used for code that is intended to stay within the context of a specific domain / application. Examples: scope:shop, scope:style-guide

## Type

Type allows for further categorization of libs and applications. The following types are supported:

### feature

type:feature is intended for Hapi plugins and complex UI components containing business logic and data-access logic.  

### data-access 

type:data-access is for libs that handle some form of data access. Examples include http services, ngrx and hapi repositories.

### util

type:util is intended for general utility libs. 

### ui

type:ui is for simple UI components where the main concern is rendering data bound UI. Components tagged as type:ui should be much less complex than components tagged as type:feature. Typical distinguishing factors include no complex business logic or direct awareness of how to fetch data. 

### Model

type:model is for plain domain objects and interfaces.

### Styles

type:styles is for pure sass libs

### Mocks

type:mock is for mocks

## build

This tag directs the [Continuous Deployment](release%20management/Continuous%20Deployment.md) pipeline on which app to deploy.

Apps should specify a "build:[name]" tag if they produce artifacts that need to be deployed.