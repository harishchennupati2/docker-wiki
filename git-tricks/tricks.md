
#

change author of last commit

`git commit --amend --author="Author Name <email@address.com>"`


change commit message of last commit

`git commit --amend`

change commit messages of last 2 commits

`git rebase -i HEAD~2`
`git rebase --amend`
`git rebase --continue`