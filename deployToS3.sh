#!/bin/bash

# any future command that fails will exit the script
set -e

npm install
npm install -g serverless

export AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID
export AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY

serverless deploy --region $AWS_REGION

