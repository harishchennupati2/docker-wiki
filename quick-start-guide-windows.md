[Home](Home.md)

## Windows Users

Follow the instructions below for setting up Docker and Git.

## Step - 0 : install system tools

### Docker

Due to complexity we have not included `Docker` support in our `setup tools` so you will need to install this manually.

You will need to setup an account with Docker Hub, but you can download Docker for free here: https://hub.docker.com/editions/community/docker-ce-desktop-windows. Don't download Docker Toolbox! It's based off an old version that handles Hyper-V differently and is incompatible with the One Site application.

#### System Requirements:

Windows \*_10 64bit: Pro_, Enterprise or Education (1607 Anniversary Update, Build 14393 or later).
Virtualization is enabled in BIOS. Typically, virtualization is enabled by default. This is different from having Hyper-V enabled. For more detail see Virtualization must be enabled in Troubleshooting.
CPU SLAT-capable feature.
At least 4GB of RAM.

### Git

If Git is not included on your system, you must first download it here: https://git-scm.com/downloads. Git installation can be difficult on a Windows system due to compatibility issues with basic tools and POSIX standards. Please follow the prompts below for the best experience.

![git-config1.PNG](https://bitbucket.org/repo/XX6G64E/images/3944755733-git-config1.PNG)

![git-config2.PNG](https://bitbucket.org/repo/XX6G64E/images/529157066-git-config2.PNG)

![git-config3.PNG](https://bitbucket.org/repo/XX6G64E/images/1978883707-git-config3.PNG)

![git-config4.PNG](https://bitbucket.org/repo/XX6G64E/images/2447909983-git-config4.PNG)

![git-config5.PNG](https://bitbucket.org/repo/XX6G64E/images/519252832-git-config5.PNG)

![git-config6.PNG](https://bitbucket.org/repo/XX6G64E/images/317190913-git-config6.PNG)

![git-config7.PNG](https://bitbucket.org/repo/XX6G64E/images/2961330501-git-config7.PNG)

### Terminal Shell Compatibility

On windows you can use `Power Shell`, or in the latest versions of windowsyou can install a true `bash` shell.

**Please** _Do Not_ use `Git Bash` for your terminal shell. `Git Bash` is not a true `bash` shell and has incompatibilities that will cause you problems during your setup and development.

## Step 1 - Clone repository

```
#!bash

git clone --recurse-submodule git@bitbucket.org:tmobile/tmo-one-site.git
cd tmo-one-site
```

## Step 2 - Initialize & Update submodules

```
#!bash
git submodule init
git submodule update
```

## Step 3 - Run webdev-setup-tools

Webdev-setup-tools is an _open source_ package we created to help setup development environments. The project configurations for `webdev-setup-tools` are stored inside the repo, hence you needed to clone the repo locally before you could run the `setup` scripts.

Using a `Powershell` window with _administrative privileges_, navigate to the cloned repo `./tools/setup` folder and run the `setup.bat`.

This `bat` script will inspect your system and alert you to dependencies and requirements that are missing or out of date, then offer to install _most_ of them for you. A few dependencies (like `java`) require manual steps from you.

The `setup.bat` script invokes an `idempotent` process. This means you can run it multiple times and the results will always be the same -- your system updated to the latest required tools and packages for development on this `mono-repo`.

## Start the application

You are now ready to launch the application! Enter

```
#!bash

npm start shop.dev
```

in the command prompt to start the application.

# Preferred IDE / Editor

There are many code editors and Integrated Development Environments.  
In order to keep our development environments compatible and all code managed similarly we ask that you choose one of the following _approved_ editors

- InteliJ
- WebStorm

  These IDEs are the _best of the best_.  
  _WebStorm_ is a subset of _IntelliJ_ that is focused on web development only.
  _IntelliJ_ supports _most_ common development languages and platforms.
  These are commercial products and will cost between $35 to$200 annually for a personal license.

- VS Code

  Microsoft's Visual Studio _Code_ is a free, open source editor that provides great support for Angular and JavaScript development.
  It also supports editing text files with LF and CRLF which makes it very useful for Windows users.
  You can download it here: https://code.visualstudio.com/download

#If all else fails
If the setup script doesn't work for you, install the needed tools manually.

* install latest version of nodejs 10.x https://nodejs.org/en/download/
* install latest version of maven https://maven.apache.org/download.cgi?Preferred=ftp://mirror.reverse.net/pub/apache/
* install latest version of java 8 https://www.oracle.com/technetwork/java/javaee/downloads/jdk8-downloads-2133151.html