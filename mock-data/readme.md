
[Home](../Home.md)

# wiki/mock-data/

these files are XHR scrapes from actual data returned to our active websites.

The _v2_ api responses are the ones we want to pay attention to at the moment, but I have included the v1 api response data for reference as well.

- [v2-api/](v2-api/readme.md)
- [audit-records.json](/assets/audit-records.json)
- [authorvalue.cell-phones-view.json](/assets/authorvalue.cell-phones-view.json)
- [cart.credit-level.good.json](/assets/cart.credit-level.good.json)
- [catalog.cell-phones.json](/assets/catalog.cell-phones.json)
- [filters.cell-phones.json](/assets/filters.cell-phones.json)
