
[Home](Home.md)

This guide provides information on how to get started in the project, and how to contribute code.

## Getting Started

Follow the instructions on the [Quickstart Guide](Quick Start Guide.md)

### Notice:
#### Please run the following command in your workspace root:

`npm config set save-prefix=''`

This will prevent npm from inserting wildcard chars into dependency versions.
eg.

`"lodash":"^3.10.5"`     ==>      `"lodash":"3.10.5"`


Next, let's start the development server.

---

## Development server

To run a development server, use this command:

```
#!bash
npm start [app].dev
```

Where `[app]` is one of the apps inside the [`apps`](https://bitbucket.org/tmobile/tmo-one-site/src/master/apps/) folder.

For example, `npm start shop.dev` will run the shop app. The application will *livereload* as you change code, so there is no need to restart the app.

**Note:** The scripts are powered by [nps](https://www.npmjs.com/package/nps). To see how they work, please look at [`package-scripts.js`](https://bitbucket.org/tmobile/tmo-one-site/src/master/package-scripts.js) (which uses [`package-scripts-utils.js`](https://bitbucket.org/tmobile/tmo-one-site/src/master/package-scripts-utils.js) for common tasks).

Next, let's see how to push code.

---

## Production Server

First, build the docker image.

```
# Compiles source with prod config, then builds docker image.
# Replace [app] with your app (e.g. shop)
K8S_ENV=prod USE_CACHED_CONFIG=true npm start [app].prod.build.with.docker
```

Second, run the server.

```
# Replace [app] with your app (e.g. shop)
npm start "[app].docker.start -e K8S_ENV=prod -e USE_CACHED_CONFIG=true"
```

**Notes:**

* The `K8S_ENV` value should match the ones in `config/apps/[app]/environments.yaml`. e.g. `prod`, `dev`, etc.
* For the `shop` app, the production value for `K8S_ENV` is `prd`, not `prod`
* The `USE_CACHED_CONFIG=true` argument instructs the Docker container to skip downloading config from S3 bucket. Instead, local config, exists in `config` directory, will be used.


### Example: Running shop in prod mode

```
K8S_ENV=prd USE_CACHED_CONFIG=true npm start shop.prod.build.with.docker
npm start "shop.docker.start -e K8S_ENV=prd -e USE_CACHED_CONFIG=true"
```

---

## Testing and Linting

When working in a project (i.e. app or lib), you can run lint and unit tests for it as follows.

```
#!bash
npm start "[project].lint "
npm start "[project].test "
```

and for UI Functional tests you need to first start your application

```#!bash
npm start "[project].dev "
npm start "[project].e2e "
```

Where `[project]` is the name as it exists in `angular.json`.

For example,


```
#!bash
npm start "shop.lint"
```

If you're not sure what the project name is, please refer to the `README.md` file inside the project, or check the mapping in `angular.json`.

---

## End-to-end Tests

Start the dev server for the app.

```
npm start [app].dev
```

Then run e2e tests when the server fully starts.

```
npm start "[app].e2e --suite='sanity'"
npm start "[app].e2e --suite='regression'"
```

npm start "[app].e2e --suite='regression' --base=''"


### In CI/CD environment

In a CI/CD environment, the `BUILD_NUMBER` environment variable should be set.

In this case, running the `npm start [app].e2e` command will use the Sauce Labs configuration instead (`wdio.sauce.conf.js`).

### Generated E2E tests

The universal app schematic provides default configuration for local and Sauce Labs, as well as one default spec.

```
npm start "app-gen --name hello-app"
npm start hello-app.dev&
npm start hello-app.e2e
```

---

Next, read the [Contributing Guide](Contributing%20Guide.md) for an outline of how you can contribute to this project.
