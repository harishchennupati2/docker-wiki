
[Home](../Home.md)

# Continuous Integration

The CI pipeline is responsible for verifying changeset (lint, tests, etc.), and updating pull request status.

[LucidChart: TOS CI Activity](https://www.lucidchart.com/documents/view/250f4fd4-636b-40f8-9ec1-2b024e3de233)
![TOS CI Activity](https://www.lucidchart.com/publicSegments/view/9d8068a9-7db4-40c6-9dcc-0439aeacb8a9/image.png)