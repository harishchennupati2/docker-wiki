
[Home](../ Home.md)

# Code Structure Change #
Modification in AEM code structure has been done to minimize the efforts for deploying AEM to any instance.
AEM code structure now has 5 modules which are as below:-

* core
* ui.common
* ui.apps.shop
* ui.apps.selfservice
* ui.apps.marketing

Out of above 5 modules, **core** and **ui.common** modules are shared among all apps eg. ui.apps.shop, ui.apps.selfservice etc.

# Code Deployment #
To build and deploy complete AEM code to any instance with

   mvn clean install -PautoInstallPackage

Or to deploy only the bundle to the author, run

   mvn clean install -PautoInstallBundle

To build and deploy only 1 apps package, go to directory for specific app and run

   mvn clean install -PautoInstallPackage

This only uploads tmobile.ui.apps.shop-<version>.zip to AEM

## Deployment to Specific Server ##
To deploy AEM to any server, run above commands to build package which will create tmobile.<module>-<version>.zip file inside target folder of every module. This .zip file can be installed to any AEM instance via package manager in CRX.

# Version Handling #
Code changes have been done to read root pom.xml version to all sub modules pom.xml. Only changing in parent pom.xml version will reflect in all sub modules. Also, for deployment if any changes are made to shared modules i.e. core and ui.common,  major version has to be updated. Otherwise all other app changes will change minor version.