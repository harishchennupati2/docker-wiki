
## Resources

To check google mozilla apple chrome add  support for both Javascript APIs and CSS rules, one could check at the following two links
- https://developer.mozilla.org/en-US/docs/Web
- https://caniuse.com/

Both are quite extensive and even mention the version of the browser which supports a particular feature.


## IE11 and Edge support in TOS

This pull request contains explanations of the various changes required to support IE11 and Edge in TOS applications.

https://bitbucket.org/tmobile/tmo-one-site/pull-requests/2946/story-cdcdwr2-1208-feature-ie11-support/diff


## Things to look out for

### Things to look out for while writing Javascript:

- Before using any native javascript API, check its browser support. There are multiple such APIs supported by most modern browsers including edge but not by IE 11.
	For example, [URL()](https://developer.mozilla.org/en-US/docs/Web/API/URL/URL) is supported by almost all browsers except IE 11 as you can see in the Browser Compatibility section of the link.
- Some APIs have partial support so it may work in some cases and may not work in others. For example [Subtle Crypto](https://developer.mozilla.org/en-US/docs/Web/API/SubtleCrypto)
- In such cases (especially when there is partial support) its better to add a polyfill or if a polyfill is not available, then avoid using it.


### Things to look out for while writing CSS:

- Angular CLI uses autoprefixer to ensure compatibility with different browsers and browser versions. Browserlist looks for configuration options in a file named _browserlist_ in the root of the project. Autoprefixer looks for the browserslist configuration when it prefixes the CSS. This configuration needs to include IE 11.

- Autoprefixer will add vendor prefixes to css not supported by some browsers.
 	- For example the shorthand flex property _flex: 1_ is not supported by IE 11 but there is a vendor prefix _-ms-flex: 1_ which is supported by IE 11. So by using autoprefixer

	<table>
		<tr><th>Original CSS</th><th>After autoprefixer</th></tr>
		<tr><td><pre>flex: 1</pre></td><td><pre>
	-webkit-box-flex: 1;
	    -ms-flex: 1;
	        flex: 1
		</pre></td></tr>
	</table>

	- *IMPORTANT* - There are some css rules which are not supported by IE 11 and there are no vendor prefixes for them too.
		For example: [_place-content_](https://developer.mozilla.org/en-US/docs/Web/CSS/place-content) . It is a shorthand for _align-content_ and _justify-content_.
		One needs to especially look out for such CSS. Check it's support and its vendor prefix availibility before using such CSS.

- CSS grids are not entirely supported by IE 11. And with angular 8 and above autoprefixer by default does not add vendor prefixes for CSS grids either. It has to be explicitly configured.
	And also the properties need to be used in a particular way for full support. Check [here](https://github.com/postcss/autoprefixer#does-autoprefixer-polyfill-grid-layout-for-ie) for all information related to CSS grid support.

- When using the following styles in an element and for IE 11
	```
	 display: flex;
	 flex-direction: column;
	```
	- in some cases height (even 1px works most of the times) is necessary, otherwise there are child elements layout issues.
	- also if there are children text elements, then an explicit width needs to be set, else the text does not wrap and could overflow
	  refer https://stackoverflow.com/questions/35111090/text-in-a-flex-container-doesnt-wrap-in-ie11

- When using _display: flex_ in an element, if that element does not have an explicit height (it could be height: 1px, along with a min-height), align-items does not seem to  work in IE 11.
  refer https://stackoverflow.com/questions/19371626/flexbox-not-centering-vertically-in-ie

- CSS _opacity_ rule does not work in IE 11 unless the element has a _display_ value set other than _inline_ and a _position_ value set other than _static_.