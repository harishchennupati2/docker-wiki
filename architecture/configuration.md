

[Home](../Home)

# Client/Server Configuration

We use a special file format based on yaml for our application configuration files.  We leverage an open source package that offers both an editor and a file hydration utility that enables us to create, store and edit highly DRY configuration files that hydrate out to wet config files that are distributed to running applications.

[Percy Cake](https:/github.com/percy-cake)

While devs are able to edit the configuraiton files directly in YAML it is not recommended as these files are space delimted and it is very easy to corrupt the file with a missing or extra space.  The Percy editor makes it easy to edit and save the files.

We use the Percy format because it supports the following features:

- inherited environments
- variable substitution
- array element clone and substitution
-