
[Home](../Home.md)


# S3 Asset Store

- [Description](#description)
- [Directory Structure](#directory-structure)

## Description

S3 (Simple Storage Solution) is used as an NFS (Network File Storage) in the cloud.
This is where we will publish static authored content and configuration files.
Our structure allows a single S3 bucket to be shared across multiple apps. While this is not required it is a convenience to publish and retrieve documents to a single file tree.

The difference is that we will have 2 S3 buckets; 1 dedicated to NPE (Non-Prod Environments) and 1 dedicated to PE (Production Environment).
This allows our publishers to push content to all environments and test them before they are released to production. This is most important with configuration files.
For more information about the risk involved when changing config files find the section below on [Configuration As Code](#Configuration-As-Code)

## Directory Structure

s3/{app*name}/\_config*/{env}/`*.json`
s3/{app*name}/\_repository*/{env}/{version}/content/tmobile/en/{app_name}/`**/*.html`

This allows for multiple applications:

```
s3/
  +-- shop/
  +-- pub/
  +-- am/
  +-- global/
  +-- join/
  +-- ...
```

within the application subtree we have 2 top level folders:

```
shop/
   +-- config
   +-- repository
```

- _config_ is where the application configuration files for every environment are stored.
- _repository_ is where the authored content for every environment is stored.

### Config

The `config` subtree holds a folder for every deployed environment.

```
shop/
   +-- config/
            +-- dev/
            +-- qat/
            +-- prod/
            +-- ...
```

within each enviromnent folder are the various configuration files expected by the application.

#### Shop Application Config Files

For Shop_Uno we have 3 basic configuration files

- client.json
- server.json
- toggles.json

## Configuration As Code

Configuration As Code is the practice of storing application runtime configuration settings as data files in a version control system like git.

There are a number of

## Repository of Authored Assets

The _repository_ folder is similar to the _config_ folder in that it contains 1 folder for every deployed environment. Within these folders will be 1 or more version folders.

```
shop/
   +-- config/
   +-- repository/
                 +-- prod/
                        +-- v1.0/
                        +-- v1.1/
                 +-- qat/
                        +-- v1.2/
                 +-- dev/
                        +-- v1.3/
```

The purpose of the version folders is to allow a rollback to an earlier version without having to syncronize the published content. The difference between to versions of the code can be a new authorable component, thus v1.1 may have the latest authorable component while `v1.0` does not. If the web producers eagerly used this new component in their designs and published to the S3 repository, and "god forbid" there was a serious error in the code that forced a rollback, all the published assets would no longer be compatible with the earlier version of the deployed code.

If, on the otherhand we want a rollback to include a complete refresh of the published content then we can remove the version folder. Unfortunately a full publish of all assets can take 15 minutes or more -- this is a long time to have production down.
