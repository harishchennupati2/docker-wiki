

# Web Security UML Diagrams

This folder contains Plant UML docusments and diagrams detailing the sequence and actrivities involved in managing secure web client sessions.

### Cold Start from CDN (sequence)

- [puml](cold_start_from_cdn.puml)
- [image](/images/cold_start_from_cdn.png)

### Pop Signature Validation (activity)

- [puml](validate_pop_signature.puml)
- [image](/images/validate_pop_signature.png)
