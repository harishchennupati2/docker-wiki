
https://medium.com/@bkawk/keeping-secrets-in-the-browser-bfd8e3d92e51

https://medium.com/asecuritysite-when-bob-met-alice/how-does-bob-share-a-secret-with-alice-without-ever-communicating-with-her-before-the-share-22320b556b10

----

https://crypto.stackexchange.com/questions/35530/where-and-how-to-store-private-keys-in-web-applications-for-private-messaging-wi

You may want to consider using the Web Cryptography API for client-side cryptography in the web browser. Then, you can create a keypair using the webcrypto api, and store the CryptoKey object, containing the user's private key, with the .extractable property set to false, using Indexed DB storage. This way the private key can only be used for decrypting and/or signing messages within the browser - but can not be read (even by client-side scripting in the browser).

For more info, see:

https://pomcor.com/2017/06/02/keys-in-browser/
https://www.w3.org/TR/WebCryptoAPI/
https://developer.mozilla.org/en-US/docs/Web/API/Web_Crypto_API
https://www.w3.org/TR/IndexedDB/
https://www.boxcryptor.com/en/blog/post/building-an-app-with-webcrypto-in-2016/
https://gist.github.com/saulshanabrook/b74984677bccd08b028b30d9968623f5
https://blog.engelke.com/2014/09/19/saving-cryptographic-keys-in-the-browser/


unless you show other users content from other users, XSS is impossible. to protect chat messages, use a sanitize function and https, and 90% of the attack vectors go away. then, add a CSP HTTP header to the page, and integrity attribs to any off-site <script> tags to stop the really cleaver attacks.

you can use tls, window.crypto, and the newer browser features like csp and subresource integrity to verify scripts. you can also use an IIFE to gen the keys and define an interface not available to later-added scripts, or use a sealed Worker to prevent arbitrary code from altering the sjcl code. lastly, if you use a user-prompted password (+b/scrypt) to feed aes, you can store the ct in localStorage w/o worry



- url:  /api/oauth
- method: POST
- cookie:
    visid_incap_850966   = ynP81BNzRsS87q2Q9qGl11pi9F0AAAAAQUIPAAAAAABziNpryFyHGoDF5WYcDVeU;
    nlbi_850966          = d47/P1jzcS2ukQKDZjoB/QAAAAAB/Jyyn8WdgMURLXsGR06K;
    incap_ses_217_850966 = KqV3bq+zXm38foQ6OfECA1pi9F0AAAAA8FgjWYw14Pu8DljhaCrFkQ==;
    tmobglobalshareddata  = %7B%22shared_id%22%3A%22eyJraWQiOiI0NDY3MzUxNy04MTc4LTJjYTMtOWU3MC1mZTZiYjg4YjU2OTIiLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJqWWZQdWNkYURmMjRVYkdLNkJ1NkFZN1N4VW5OY2I3SiIsInJ0Ijoie1wic2VnbWVudGF0aW9uSWRcIjpcIlBPTEFSSVNcIn0iLCJkZWFsZXJDb2RlIjoiIiwiaXNzIjoiaHR0cHM6XC9cL2FwaS50LW1vYmlsZS5jb21cL29hdXRoMlwvdjQiLCJtYXN0ZXJEZWFsZXJDb2RlIjoiIiwiYXV0aFRpbWUiOiIxNTc2Mjk3MDUxNjc0Iiwic3RvcmVJZCI6IiIsInVzbiI6ImNiNWU0ZmMwLTU2ODAtZjc2Ny1jMzg4LTdjMTkxMmY0ZTgwOSIsImF1ZCI6ImpZZlB1Y2RhRGYyNFViR0s2QnU2QVk3U3hVbk5jYjdKIiwic2VuZGVySWQiOiIiLCJuYmYiOjE1NzYyOTcwNTEsInNjb3BlIjoiIiwiYXBwbGljYXRpb25JZCI6IiIsImV4cCI6MTU3NjkwMTg1MSwiaWF0IjoxNTc2Mjk3MDUxLCJjaGFubmVsSWQiOiIiLCJqdGkiOiI1ZDg1Zjc5ZS03Yjc5LTBjMDgtNjc5MS1jNzAyYjI5ZDQ1YWEifQ.A7lGRzP6aYC3uTnxMNPNMt3c7UVMg42UhSKPt8CSrZYvG0stpi_fRUo7KzO_idfpnASIM-OE66-OkTYCSQk8KZXLysizAZSnOXJamnAzPleP-g_D3HHeS1IiuJjURoFoKKSka8CTKo8TcCeeoHt13hUe0hdX4MhJ9tMztMfYt0gzxtzbBnvPM7Od-6PR6-CLHLsV8GNsYUS0tyJ-lOE1uDj8szOabf5WSHGTFWM7qvbBcVxRw0LoKw-BsYrt0nofb5itEkfHq0QEVp-tCRgFNZHejlb4CIcHtqZAl6moHd-ysygCMVC6tsw7bYwr_lHoYt4c2yyfGDhQYg72lNj0kQ%22%2C%22creditProfileId%22%3A%22ifbecmzsg42dgljxgq3doljsifcdelkggeydiljugzcdambqiyydcmsfga%3D%22%2C%22profileId%22%3A%22mm3tiyztmi3dgljyga4diljuhe4gcllbge3gcljsha4dmyzymzrtqnlggu%3D%22%2C%22profileAccountId%22%3A%22hfbeinsfha4eiljxiiyeiljtgbbukljxge3ecljsizdegnrug5bdorcfgu%3D%22%7D

Why do we have a cookie in t-mobile.com storing a shared_id ?
{
  "shared_id": "eyJraWQiOiI0NDY3MzUxNy04MTc4LTJjYTMtOWU3MC1mZTZiYjg4YjU2OTIiLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJqWWZQdWNkYURmMjRVYkdLNkJ1NkFZN1N4VW5OY2I3SiIsInJ0Ijoie1wic2VnbWVudGF0aW9uSWRcIjpcIlBPTEFSSVNcIn0iLCJkZWFsZXJDb2RlIjoiIiwiaXNzIjoiaHR0cHM6XC9cL2FwaS50LW1vYmlsZS5jb21cL29hdXRoMlwvdjQiLCJtYXN0ZXJEZWFsZXJDb2RlIjoiIiwiYXV0aFRpbWUiOiIxNTc2Mjk3MDUxNjc0Iiwic3RvcmVJZCI6IiIsInVzbiI6ImNiNWU0ZmMwLTU2ODAtZjc2Ny1jMzg4LTdjMTkxMmY0ZTgwOSIsImF1ZCI6ImpZZlB1Y2RhRGYyNFViR0s2QnU2QVk3U3hVbk5jYjdKIiwic2VuZGVySWQiOiIiLCJuYmYiOjE1NzYyOTcwNTEsInNjb3BlIjoiIiwiYXBwbGljYXRpb25JZCI6IiIsImV4cCI6MTU3NjkwMTg1MSwiaWF0IjoxNTc2Mjk3MDUxLCJjaGFubmVsSWQiOiIiLCJqdGkiOiI1ZDg1Zjc5ZS03Yjc5LTBjMDgtNjc5MS1jNzAyYjI5ZDQ1YWEifQ.A7lGRzP6aYC3uTnxMNPNMt3c7UVMg42UhSKPt8CSrZYvG0stpi_fRUo7KzO_idfpnASIM-OE66-OkTYCSQk8KZXLysizAZSnOXJamnAzPleP-g_D3HHeS1IiuJjURoFoKKSka8CTKo8TcCeeoHt13hUe0hdX4MhJ9tMztMfYt0gzxtzbBnvPM7Od-6PR6-CLHLsV8GNsYUS0tyJ-lOE1uDj8szOabf5WSHGTFWM7qvbBcVxRw0LoKw-BsYrt0nofb5itEkfHq0QEVp-tCRgFNZHejlb4CIcHtqZAl6moHd-ysygCMVC6tsw7bYwr_lHoYt4c2yyfGDhQYg72lNj0kQ",
  "creditProfileId": "ifbecmzsg42dgljxgq3doljsifcdelkggeydiljugzcdambqiyydcmsfga=",
  "profileId": "mm3tiyztmi3dgljyga4diljuhe4gcllbge3gcljsha4dmyzymzrtqnlggu=",
  "profileAccountId": "hfbeinsfha4eiljxiiyeiljtgbbukljxge3ecljsizdegnrug5bdorcfgu="
}

- origin: https://www.t-mobile.com
- referer: https://www.t-mobile.com/cell-phones
- sec-fetch-mode: cors
- sec-fetch-site: same-origin
- body:

```
{
   "jwk":{
      "alg":"RS256",
      "e":"AQAB",
      "ext":true,
      "key_ops":["verify"],
      "kty":"RSA",
      "n":"07_RdpigwfW2zaxASAA6DcoeJkH449SnEckcqDYiu_5qJGqJUb1lmjN4NdkFu1ux6SDFc-rsXbffLfZBvMX7_LR7DLzCtv8AI1LB_ncsymm63xV4W9_HUl0iLfUBbA45xTeQhmFy4B2c4B1ZPxC0jeHVj8s8yQ9uCR-KFLablg1Nip16FBfFwU7YXXTmacyrN-f5YdZbQ_u-H2PuFwFFDuOhk8sByi4KbjjtVP4hvAKBtQhB6lAGCT9YdSsxdX7W3xWx0TfGjTfD1zdEflqbjQ3gwkaGham3k3hQ9HEb1gnfnkXVj5T_eJjcuEETrUk78fxnCwRRN5blIymPZSyGdw"
   }
}
