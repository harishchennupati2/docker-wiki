

|FIELD1     |USED FOR     |CLIENT     |Eservice          |Apigee        |EOS micro        |Domain micro
|---------|-----------------|--------|--------------|------------------|---------------|----------------------|---|---|---|---|---|---|
| ISS        | ISSUER of this JWT token.      |      | Only honors tokens from eui/iam   | SHOULD have a whitelist of accepted token issuers.    |   |   |
| IAT        | Issued At time.      |      |       | |   |   |
| EXP        | Expire time  | Fetch a new token when expired.  | Don’t allow request after the id token is expired.  | Don’t allow request after the id token is expired. When caching a token, cache only till exp.     | Don’t allow request after the id token is expired.      | Don’t allow request after the id token is expired.      |
| AUD        | Audience. This will be The CLIENT ID of the client. We will be pushing for Apigee to get provisioning api’s to PUSH client ID’s into IAM.  |      |       | This should eventually match the partner id in apigee.  Apigee can use this to the monetization features.   |   |   |
| AUTH_time  | This timestamp is the time the user authenticated.  The user may have an SSO session that started 30 minutes ago. And just entered this site. So the auth time is 30 min ago, and the IAT is now for this service. |      |       | |   |   |
| AT | This is the OLD fashion Access TOKEN. This can be used to query the full 30 pages of JSON about this user.     |      | Eservice uses this to get full userinfo.  | |   | IF the domain micro service needs to know more about who this user is an what their permissions are, the service should use the AT to fetch the full user profile.  |
| SUB        | This is the subject identifier. This is the IAM UUID for the user.  Chub enables you to search for chub records using this UUID.  A UUID identifies a unique person. | **log this number**        | **log this number** | **log this number**   | **log this number**     | IF the domain micro service needs to know more about this user. The DOMAIN can use this UUID to query CHUB!!! |
|    |    |      |       | |   |   |
|    | ALL services that see this JWT must log this UUID into splunk along with the USN.    |      |       | |   |   |
|    |    |      |       | |   | **log this number**     |
| RT | Routing flags. These will enable you to flag traffic for testing… coordinate with Engan if you are interested. |      |       | |   |   |
| CNF        | CNF is the PUBLIC key that this ID_TOKEN has been issued TOO.  Services MUST use this public key to validate the signature on any POP token.       |      |       | Use this to validate the POP        |   |   |
| USN        | UNIQUE session Number.         | **log this number**        | **log this number** | **log this number**   | **log this number**     | **log this number**     |
|    | ALL services that see this JWT, MUST log the USN into SPLUNK transaction LOGS!!!!!!  |      |       | |   |   |
| ACR        | This informs you of the security level this users authentication has reached. For instance LOA3 means the use has achieved 2nd factor since the AUTH_TIME listed above.        |      |       | | If the service is secured. (ie… sim swap, add a line, porting).   Then require atleast LOA3 | If the service is secured. (ie… sim swap, add a line, porting).   Then require atleast LOA3 |
| AMR        |    |      |       | |   |   |
| ENT_TYPE   | For services that need to know the basic rols for the user, look for entitlement type basic. |      |       | |   |   |
| ENT        | Basic entitlements are included here.  |      |       | | Use this to decide if the user can make this api call.  |   |
| Acct : id  | This is the BAN/FAN ID…        |      |       | | Use this to decide if the user can make this api call.  | |
| Acct: r    | This is the logged in users ROLE to this BAN/FAN |      |       | | Use this to decide if the user can make this api call.  |  |
| Acct : lines : phnum | This is the ARRAY of phone lines on the BAN/FAN  |      |       | | Use this to decide if the user can make this api call.  |  |
| Acct: lines: r       | This is the logged in users ROLE to each Phone line.     |      |       | | Use this to decide if the user can make this api call.  |  |
|    | D : Default line     |      |       | |   |   |
|    | RL : direct linked line.      |


```
{
  "iat": 1554927171,                                   # Issued At Time
  "exp": 1554930770,                                   # Expire time
  "iss": "https://ppd.brass.account.t-mobile.com",     # issuer: uri of entity that issued the token
  "aud": "TMOApp",                                     # Audience: Client Id of the target application
  "AT": "01.USR.rD4OngvQvqneKWaMU",                    # Old fashion "Access Token"
  "sub": "U-ec1c0e2d-7455-48bb-8096-8bb4112d53d7",     # Subject id:  this is the IAM UUID for the user
  "acr": "loa2",                                       # Session Security level: LOA3 == user has achieved 2nd factor since AUTH_TIME
  "cnf": "..."                                         # Confirmation: This is the public-key that this id_token has been issued to
  "amr": [
    "password",
    "security_question"
  ],
  "usn": "8083b9083ed3210c",                           # Unique Session Number:  ** Log this number **
  "ent_type": "direct",                                # Entitlement Type: [ basic | direct | ... ]
  "ent": {                                             # Entitlements : Basic entitlements.
    "acct": [
      {
        "r": "AO",                                     # Role : logged in users role to this BAN/FAN
        "id": "965807897",                             # ID of this BAN/FAN
        "tst": "IR",                                   #
        "line_count": 2,
        "lines": [
          {
            "phnum": "4254351249",                     # a PHone NUMber on the BAN/FAN
            "r": "D"                                   # [ D == Direct Line,  RL == direct Linked Line ]
          }
        ]
      },
      {
        "r": "AO",
        "id": "966052363",
        "tst": "IR",
        "line_count": 2,
        "lines": [
          {
            "phnum": "4253899152"
          }
        ]
      }
    ]
  }
}
```

| claim            | description                                                              |
|------------------|--------------------------------------------------------------------------|
| iss              | ISSuer of this JWT token.  i.e. "https://ppd.brass.account.t-mobile.com" |
| iat              | Issued At time.                                                          |
| exp              | Expire time                                                              |
| aud              |                                                                          |
| auth_time        |                                                                          |
| at               |                                                                          |
| sub              |                                                                          |
| rt               |                                                                          |
| cnf              |                                                                          |
| usn              |                                                                          |
| acr              |                                                                          |
| amr              |                                                                          |
| ent_type         |                                                                          |
| ent              |                                                                          |
| acct.id          |                                                                          |
| acct.r           |                                                                          |
| acct.lines.phnum |                                                                          |
| acct.lines.r     |                                                                          |
|                  |                                                                          |

### exp
Fetch a new token when expired. 
Don’t allow request after the id token is expired. 
Don’t allow request after the id token is expired. 
When caching a token, cache only till exp. 
Don’t allow request after the id token is expired. 
Don’t allow request after the id token is expired.                                         |





### "iat"

### "exp"

### "aud"

### "auth_time"

##


# "at"

### "sub"





## POP (nonce)
```
{
   "ehts": LIST of headers/body/url that will be sha256 hashed.
   "edts": Sha256 hash of the listed fields in the listed order.
}

```

--  If this is a GET, then require uri is included in ehts
--  If this is a POST then Require body is included in ehts. 

