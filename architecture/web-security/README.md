
# Web Security and Authentication

```
NOTE:

Add check to validate commit message that local commit cannot happen to master branch

```


How do we manage a users' data ( either authenticated or anonymous ) across page requests ( full http page load )?

How do we identify an authenticated user both on the client and on the server without exposing the user's credentials (once they are logged in)?


# A _not so brief_ history of web security

## Server-Centric Session Management

Early web application user session management was implemented using hidden form fields that we included in any POST action back to the server.

Later web servers included server side session management where they stored user data on the server and added a cookie to the page request as a key for that users specific data .

`JSESSIONID: '123585692349'`

The session state included the application history and user settings as well as the authenticated user id (if any). This allowed the user to authenticate and login to a page and keep a running session across page requests.

The problem with this is that server-centric session managment was scalability. IF the application or site was being served by a single web-server then serve-side session management works fine, as long as you can veritically scale that server to handle the increased load as the site/app gains popularity.

Once your site requires horizontal scaling then server-side session management becomes a challenge.  When a user first requests a page they are connected, thru a load balancer, to one of many servers in a cluster or farm. From that moment forward they are required to connect to that same server so as to retain their session state continuity.



## Server-Side Session Management

In order to make this server-side session management more scalable architects added a central database that would store the session data, identified by the session id cooikie stored with the browser ( e.g. `JSESSIONID`). This cookie can be locked down to the path or domain for your web application so only your application, or apps on your domain, can access the cookie contents.

As web security improved over the years browsers offered more secure ways to store and access these special files. The HTTP-ONLY cookie does not allow javascript running on a client to access the cookie value at all, the contents thereby being only visible on the server.  This kind of cookie is transmitted from the client to the server on every request.  The server has access to these cookies and can use the values to look up a user's authorization levels before performing any actions.

This architecture is more scalable than the server-centric session-management but exposes a bottleneck on access to the database server that stores the session data, along with a latency factor for accessing the stored session data.

## Web Security Tokens

The _JSON Web Token_ (`JWT`), pronouned **_jot_**, has become the recognized standard for authenticated web session identification.  This token format has 3 parts

`header.payload.signature`

The **header** defines the format of the token and what hashing algorithm was used to sign it.
The **payload** is a simple Javascript Object Notation (`JSON`) node that can contain any serializable data you want.
The **signature** is a hash of the `header.payload` contents.

Since we are using a single signing authority to sign all tokens (TMS) the correct term for this token is _JSON Web Signature_ (`JWS`).

validating a `JWS` is a complex, yet well known, process that is better described [here](https://auth0.com/docs/tokens/guides/jwt/validate-jwt#manually-implement-the-checks)

Once the token has been validated the contents can be garaunteed to be un-modified since the token was last signed.  This does *not* garauntee that the entity using the token is the same entity that the token was assigned to.

### Man in the middle attack

**Scenario**: somone is _sniffing_ the line you are using to communicate with a backend server and intercepts either a `request` or a `response` that contains your `token`.  They can extract the `token` and use it to spoof calls with your identity.  With some sites this gives them access to your account, or allows them to purchase items, change profile info, etc.
-- basically - you've been hacked!

## Proof-Of-Possession (`POP`)

The `POP` was created as a request signature.  The pop-signature verifies that **only** the current users' browser instance could have **possibly** made the request. A 3rd party may still be able to grab your token _in-flight_ but will be unable to use it as they cannot sign the request with a valid `POP`.

### What is a `POP` token

a `POP` token is a `JWT` that has been signed by the client.  This means that the client browser must generate an acyclic crypto key to sign the POP JWT with.  When the client app wants to make a request to a backend server or web api it signs the request with a `POP` Signature.

The `POP` contains 2 basic fields:

```
{
  ehts: [],
  edts: '...'
}
```


and is signed using a private key stored on the browser.  Because the private key of a PGP pair is *not* serializable it cannot be transmitted over HTTP, thus it cannot be stored in localStorage, sessionStorage, cookies or request headers.  This is good.  We want the browser private key to be created locally and never leave.  This simple fact means even if a 3rd party script (XSS) were able to see your private key on your local machine, they could not transmit this key anywhere else.

>Note:  an extremely eggregious security hole is that an XSS  (3rd party script injected onto your site) **can** lift your keys (if they know where to find them) and sign a new token with whatever contents it decides.

To create a pop you must first decide which parts of your request you want to secure.  This is commonly fields such as an identity token (`JWS`), the `url` and perhaps the request `body`. Add these three keys to the `eiht`, then concatenate the header values associated with those keys and hash them using a 256SHA hash algorithm.  This should result in a 64 byte hexadecimal string -- a request fingerprint.



## Stateless Serverless architecture with SSO



# Transitioning from Server Side Session Managment


## Problem statement
We have a CDN which serves our UNO application to users. This CDN can only generate one response, it cannot disambiguate between authenticated/unauthenticated calls. Thus we need to make use of the CDN’s response for both unauthenticated/authenticated users.

## Potential solutions - Authentication state
Currently  a user’s auth state is captured within an HTTP header sent along with the request to the CDN. Our primary use case we are targeting is when Imperva delivers the `index.html` response to the client. In this scenario, making use of response headers is not feasible as there is not receiving javascript that has a reference to the request object. Thus our solutions will try to target using cookies sent along with the `index.html` document. However, this header is ignored by the CDN and dropped from the response. We can resolve this in one of two ways:

1. Our CDN, Imperva, exposes a set of “Rules” which can be leveraged to implement our own security, delivery, and access control rules on top of Imperva's existing security and application delivery logic. These rules are capable of settings different headers and cookies in the response.

1. Enforce our requesters, ie the entities requesting the index.html page, send over authentication state as a cookie as part of the request rather than as a request header. Then Imperva should send those cookies, as is, to the browser.

At a high level, we are exploring solutions which involve having authentication state be delivered to the browser within cookies.  These would be readable by out application js (once it is loaded, parsed and initialized).

Potential Solution - Translating cookies to JWT key pairs
Our existing webworker is setup to check for an existing key pair within `indexedDB`, and will update it accordingly if one exists and is still valid. If we can populate `indexedDB` with a keypair, we can reuse that logic.

When `index.html` is delivered to the browser where it is parsed:
Create a small inline `<script>` in the `index.html` `<head>` that is set to run first, This should be parsed first and executed before any other DOM is parsed or asset XHR are executed.
This inline `<script>` would extracts the pop-signature cookies (if they exists) and push to `indexedDB`.
After extraction, delete the cookies so they are not transmitted with further XHR.
Some browsers do not support `indexedDB` (for example _Firefox_ in incognito mode) so we should also consider extracting the cookie in a way that the main application thread can read easily and augment our existing key generation logic on the main thread to check that location.
Context: About our existing Web-Worker
```
self.keyPair = null;

self.addEventListener('message', function(e) {
 if (!self.crypto || !self.crypto.subtle) {
   self.postMessage(null); // Signal to generate keys on main thread
 } else if (self.keyPair) {
   self.postMessage(self.keyPair);
 }
});
function generateKeys() {...}

function ensureKeys() {...}

if (self.crypto && self.crypto.subtle) {
   ensureKeys();
}
```

The web-worker script self initializes **if** the browser supports crypto in a web worker ( Currently IE 11 does not).

```
if (self.crypto && self.crypto.subtle) {
  ensureKeys();
}
```

If the browser exposes the crypto library to the web-worker thread, and that lib has the crypto.subtle object then the web-worker can generate keys.  If not, then the main rendering thread must access these objects and generate the keys.

This, however is not optimized to the possibility that a browser does not expose the crypto library to the web-worker, but it does have access to the indexDB.  In this scenario the main render thread could have generated the keys and stored them in IndexDB, then a subsequent page load could simply ask for the keys to be retrieved.

The script registers an event listener to respond with the crypto key pair (if self.crypto enabled) or to trigger retrieval/generation of crypto pair in the main thread (because specific browser does not support worker thread access to cryptography or IndexDb.


## Security Issues

https://dzone.com/articles/cookies-vs-tokens-the-definitive-guide

https://odino.org/security-hardening-http-cookies/

https://www.quora.com/What-are-some-of-the-security-issues-surrounding-internet-cookies

https://techblog.topdesk.com/security/cookie-security/


A `dat` contains the app `cnf` and a `usn`, the same values are also stored in the `idt`.
Both the `dat` and the `idt` are signed by TMS.

If the `dat` and the `idt` both validate against the current TMS public-key then their contents are ‘un-molested’.

After validating the `dat` and `idt` we check that the `cnf` and `usn` are identical, if they are then we are assured that these tokens were sent from our application.

Finally we can check the `aud` against our applicaiton’s client-id.  If these tokens were included as a handoff from one app to another then the values will not match.

Next we have the `pop` that signs the request.  We use the ‘cnf’ from the `dat` or the `idt` to validate the `pop`.  Once validated we can calculate and match the request fingerprint inside the `pop` to validate that the entire request is unmolested.



When the CDN reads incoming header values it creates response header values of type `Set-Cookie` that instructs the browser to create a `cookie` file on the browser local file system.
This file can have a set expiry, after which it will be deleted, or it can be session based and will delete when the browser (tab) is closed.

If a handoff request is sent from the mobile app:


## Sample Page Request from browser nav

### General

**Request URL**: https://www.t-mobile.com/cell-phones
**Request Method**: GET
**Status Code**: 200
**Remote Address**: 45.60.11.176:443
**Referrer Policy**: no-referrer-when-downgrade

### Request Headers
>**authority**: www.t-mobile.com
>**method**: GET
>**path**: /cell-phones
>**scheme**: https
>**accept**:`text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3`
>**accept-encoding**: `gzip, deflate, br`
>**accept-language**: `en-US,en;q=0.9`
>**cache-control**: `no-cache`
>**dat**: `abc`
>**idt**: `def`
>**pop**: `xyz`
>**cookie**:
>>_**gcl_au**=`1.1.905605537.1560382182;`
>>**_fbp**=`fb.1.1560382187213.2032195834; **visid_incap_850966**=9Dcz/4d6Rpi478g6xYMs4u6LAV0AAAAAQUIPAAAAAADxagWbOrwulsmMvBco2Wgo;`
>>**BVBRANDID**=`3628e849-7416-49d8-a7f3-e1fdc29ff052;`
>>**TMobileSegmentation**=`UserId=1d41ad92-4ade-4522-9d8a-ac58b835a4d2;`
>>**tmenv**=`postpaid;`
>>**_4c_mc_**=`e4e49aba-78f6-4412-9f86-18aaff1225f4; **_ga**=GA1.2.1209023683.1560880203;`
>>**visid_incap_385986**=`FWQeCVquS9mENmkLobeR1fjfC10AAAAAQUIPAAAAAABk4LJAnHsLLktwAAVZYFQZ;`
>>**MP_LANG**=en;
>>**rxVisitor**=`1559695176449TS81OTUAEUIGDF49A0IN6N2H6661T2P2;`
>>**WRUIDAWS**=`2311439290646596;`
>>**TMobileUSStore**=`MarketUniqueID=52535b69-3a23-4654-95c4-1d2c3ab99269&MarketCode=WDC&NeighborhoodName=DC%2fBaltimore&StateAbbreviation=DC&CityName=WSHNGTNZN1&StateName=District+of+Columbia&ZIP=20007;`
>>LPVID=`E4ZTVhYjU5YWZiM2ZhMGMz;`
>>_derived_epik=`dj0yJnU9ZHZtNk9qNGRHRDIyODdkOWJmY040NU8xWUZ2Ukt2RDYmbj1iNnV4b2JXM0dUSjNOb3JlU1l0clJRJm09NyZ0PUFBQUFBRjBqekxB;`
>>**_scid**=`eaa081b7-7737-43d0-b639-85252ad3fb4a;`
>>**s_visbtwpur**=1;
>>**s_ecid**=`MCMID%7C50744776210450081220994481643022310534;`
>>**mp_dev_mixpanel**=`%7B%22distinct_id%22%3A%20%2216b481c24f87eb-02e039a42cdf0a-37647e03-1fa400-16b481c24f9c57%22%2C%22bc_persist_updated%22%3A%201564440418453%7D;`
>>**lpc**=n;
>>**aam_uuid**=`50484021036894947200985927388758805180;`
>>**AAMC_tmobile_0**=`REGION%7C9%7CAMSYNCSOP%7C%7CAMSYNCS%7C;`
>>**_cs_ex**=1;
>>**_cs_c**=1;
>>**mtc**=`%7B%22m%22%3A%22c%3A0%2Ca%3A0%2Cl%3A0%2Cv%3A0%22%2C%22t%22%3A%22c%3A0%2Ca%3A0%2Cl%3A0%2Cv%3A1%22%7D;`
>>**_sctr**=`1|1566457200000;`
>>**mbox**=`PC#392bbbd9868144408fbc4e7b1f30552c.17_222#1629744305|session#1aa9037e72c74e91b6d80afe1a5d0d05#1566500541;`
>>**mp_t_mobile_mixpanel**=`%7B%22distinct_id%22%3A%20%2216c977a2f806cc-034f03d5273b46-37607c02-1fa400-16c977a2f8172a%22%2C%22bc_persist_updated%22%3A%201566498682863%7D;`
>>**ctm**=`{'pgv':5194252181836822|'vst':6895113906203595|'vstr':8438572550494564|'intr':1566499506264|'v':1|'lvst':2759};`
>>**__CT_Data**=`gpv=15&ckp=tld&dm=t-mobile.com&apv_8_www42=15&cpv_8_www42=15&rpv_8_www42=15;`
>>**ClicktaleReplayLink**=`https://dmz01.app.clicktale.com/Player.aspx?PID=8&UID=2311439290646596&SID=2399094482141472;
**utag_main**=v_id:016c3fea4cac001c45c5bba8014903079002e07100bd0$_sn:6$_se:5$_ss:0$_st:1566501306791$vapi_domain:t-mobile.com$ses_id:1566498680668%3Bexp-session$_pn:2%3Bexp-session;`
>>**AMCV_1358406C534BC94D0A490D4D%40AdobeOrg**=`-330454231%7CMCIDTS%7C18131%7CMCMID%7C50744776210450081220994481643022310534%7CMCAAMLH-1567104306%7C9%7CMCAAMB-1567104306%7CRKhpRz8krg2tLO6pguXWp5olkAcUniQYPHaMWWgdJ3xzPWQmdj0y%7CMCOPTOUT-1566505880s%7CNONE%7CMCAID%7CNONE%7CMCCIDH%7C1260738743%7CvVersion%7C3.1.2`

>**pragma**: no-cache
>**sec-fetch-mode**: navigate
>**sec-fetch-site**: none
>**sec-fetch-user**: ?1
>**upgrade-insecure-requests**: 1
>**user-agent**: `Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36`


### Response Headers
**cache-control**: `no-cache`
**content-encoding**: `gzip`
**content-type**: `text/html; charset=utf-8`
**date**: `Tue, 17 Sep 2019 20:53:21 GMT`
**server**: `openresty/1.15.8.1`
**set-cookie**: `nlbi_850966=wSe+fTktjnR2NC2rZjoB/QAAAAAswXuvfBNgXuvRDcPOCTs0; path=/; Domain=.t-mobile.com`
**set-cookie**: `incap_ses_235_850966=IxDqA1HM0mWCjtJrOQRDA8BHgV0AAAAAoQgUpwzx2mBhKlLaEUExfQ==; path=/; Domain=.t-mobile.com`
**status**: 200
**strict-transport-security**: `max-age=15724800; includeSubDomains`
**vary**: `accept-encoding`
**x-cdn**: `Incapsula`
**x-iinfo**: `4-32936925-32936926 NNNN CT(0 0 0) RT(1568753599668 0) q(0 0 0 -1) r(9 9) U12`


-----
### Response Cookies

**Name**:	`incap_ses_235_850966`
**Value**:	`IxDqA1HM0mWCjtJrOQRDA8BHgV0AAAAAoQgUpwzx2mBhKlLaEUExfQ==`
**Domain**:	`.t-mobile.com`
**Path**:	`/`
**Expire**:	`Session`
**Size**:	`107`

**Name**:	`nlbi_850966`
**Value**:	`wSe+fTktjnR2NC2rZjoB/QAAAAAswXuvfBNgXuvRDcPOCTs0`
**Domain**:	`.t-mobile.com`
**Path**:	`/`
**Expire**:	`Session`
**Size**:	`91`


