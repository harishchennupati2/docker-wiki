
[Home](Home.md)

# How to handle login tango

add memoized public method to parent window for return message. memoized token that must be inside return request to be valid.

open dialog window (modal) with iframe that loads external login page
add add return url to local login/index.html

login.index.html loads simple login.js

login.js executes window.login({pat: pre-auth-token, lt: iframe-token})

window.login
validates memoized token.
passes pre-auth-token to service to get jwt
removes window.login
